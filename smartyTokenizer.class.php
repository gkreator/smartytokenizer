<?php
class SmartyTokenizer{
    /**
     * holds all tokens
     * @var array
     */
    private $tokens_array = array();
    /**
     * constructor
     *
     * @param Smarty $smarty Smarty Object
     * @param string $_content template file contents
     * @return null
     */
    public function __construct(Smarty $smarty, $_content){
        $compiler = new Smarty_Internal_SmartyTemplateCompiler('Smarty_Internal_Templatelexer', 'Smarty_Internal_Templateparser', $smarty);
        $lex = new Smarty_Internal_Templatelexer($_content, $compiler);
        while ($lex->yylex()){
            array_push($this->tokens_array, array(
                'token_id'=>$lex->token,
                'value'=>$lex->value,
                'line'=>$lex->line
            ));
        }
    }
    /**
     * gets teh token array
     *
     * @return array
     */
    public function getTokens(){
        return $this->tokens_array;
    }
    /**
     * gets all array variables
     *
     * @return array
     */
    public function getAllArrayVars(){
        $tokenCount = count($this->getTokens());
        $t18=false; //$
        $t19=false; //varName
        $t28=false; //foreach init
        $t35=false; //from=
        for($i=0; $i<$tokenCount; $i++){
            switch($this->tokens_array[$i]['token_id']){
                case 28:
                    $t28=true;
                break;
                case 35:
                    if($t28){
                        $t35=true;
                        $t28=false;
                    }
                break;
                case 18:
                    if($t35){
                        $t18=true;
                        $t35=false;
                    }
                break;
                case 19:
                    if($t18){
                        echo $this->tokens_array[$i]['value'].'<br />';
                        $t18=false;
                    }
                break;
                default:
                    $t18=false;
                    $t19=false;
                    $t28=false;
                    $t35=false;
                break;
            }
        }
    }
    /**
     * gets all variables
     *
     * @return array
     */
    public function getAllVars(){
        $tokenCount = count($this->getTokens());
        $t18=false; //$
        $t19=false; //varName
        for($i=0; $i<$tokenCount; $i++){
            switch($this->tokens_array[$i]['token_id']){
                case 18:
                    $t18=true;
                    $t19=false;
                break;
                case 19:
                    $t19=true;
                    if($t19 && $t18){
                        echo $this->tokens_array[$i]['value'].'<br />';
                    }
                    $t18=false;
                break;
                default:
                    $t18=false;
                    $t19=false;
                break;
            }
        }
    }
}
?>